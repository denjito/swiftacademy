var menuArr,
	menu2Arr,
	menuLiId,
	i, j, len, lend,
	headerButton,
	menuAll,
	headerLi,
	headerUl,
	dropMenu,
	isClicked,
	isClicked2,
	isClicked3,
	dropImg,
	dropClose,
	dropGet,
	dropInactive,
	divDropMenu,
	slide;

//HeaderMenu
function headerMenu (){
	headerButton = document.getElementById ('headerButton');
	
	isClicked = false;
	headerButton.addEventListener ('click', function(){
		headerLi = document.getElementsByClassName ('liSlide');
		menuAll = document.getElementById ('all');
		dropMenu = document.getElementById ('drop');
		headerUl = document.getElementById ('ulSlide');
		headerUl.className = 'slide';
		menuAll.className = 'menuAll';
		dropMenu.className = 'dropMenu';
		isClicked = (isClicked)?false:true;
		if(isClicked){
			headerUl.className += ' headerSlideNone';
			menuAll.className += ' menuAllMarginN';
			this.src = 'images/plus.png';
			dropMenu.className += ' headerDropN';
			setTimeout(function(){
				for (i = 0, len = headerLi.length; i < len; i++){
				headerLi[i].style.display = 'none';
				menuAll.style.marginTop = '40px';
				dropMenu.style.top = '246px';
			}
			}, 200);
		}else {
			for (i = 0, len = headerLi.length; i < len; i++){
				headerLi[i].style.display = 'block';
			}	
			headerUl.className += ' headerSlideBlock';
			menuAll.className += ' menuAllMarginB';
			dropMenu.className += ' headerDropB';
			this.src = 'images/x.png';
			setTimeout(function(){
				menuAll.style.marginTop = '140px';
				dropMenu.style.top = '346px';
			}, 200);
		}
		
	},false);
}	

//MenuOptions	
function menu (){
menuArr = document.querySelectorAll('nav.center2 ul li p');
for(i = 0, len = menuArr.length; i < len; i++){
	menuArr[i].addEventListener('click', function(){
			menu2Arr = document.getElementsByClassName ('menu2');
			menuArr = document.querySelectorAll('nav.center2 ul li p');
			for (j = 0, lend = menu2Arr.length; j < lend; j++){
				menu2Arr[j].style.display = 'none';
				menuArr[j].style.color = '#cacacb';
			}
			menuLiId = this.id;
			this.style.color = '#eeea87';
			menuLiId = menuLiId.replace('menu_','');
			menu2Arr[menuLiId].style.display = 'block';
	},false);
}
}

//DropMenu
function dropMenu (){
	dropImg = document.getElementById('dropImg');
	dropClose = document.getElementById ('dropClose');
	isClicked = false;
	dropImg.addEventListener ('click', function(){
		dropInactive = document.getElementById('dropInact');
		divDropMenu = document.getElementById ('drop');
		divDropMenu.className = 'dropMenu';
		isClicked2 = (isClicked2)?false:true;
		if (isClicked2){
			dropInactive.style.display = 'block';
			this.src = 'images/dropx.png';
			divDropMenu.className += ' dropSlideBlock';
		}else {
			this.src = 'images/dropPlus.png';
			divDropMenu.className += ' dropSlideNone';
			setTimeout(function(){dropInactive.style.display = 'none'; }, 200);
		}
},false);
		dropClose.addEventListener ('click', function(){
		dropGet = document.getElementById('dropGet');
		isClicked3 = (isClicked3)?false:true;
		if (isClicked3){
			dropGet.style.display = 'none';
			this.innerText = "Open";
		}else {
			dropGet.style.display = 'block';
			this.innerText = "Close";
		}	
},false);	
}


$(document).ready(function() {
	$("#owl-example").owlCarousel({items: 4});
	menu();
	headerMenu();
	dropMenu();
});