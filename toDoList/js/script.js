var listItem,
	addText,
	btnAdd,
	checkBox,
	span,
	date,
	id,
	ask,
	rename,
	checkBoxId,
	spanText,
	btnDel,
	r,
	list;
function createNewItem () {
	addText = document.getElementById ('addText');
	btnAdd = document.getElementById ('buttonAdd');
	list = document.getElementById ('list');
	
	addText.focus();
	
	//Add with Click
	btnAdd.addEventListener ('click', function(){
	if(addText.value == '' || addText.value == ' '){
		addText.select();
		return false;
	}
	listItem = document.createElement ('li');
	listItem.id = 'li_'+ id;
	list.appendChild(listItem);
	date= new Date();
	id = '' + date.getHours() + date.getMinutes() + date.getSeconds() + date.getMilliseconds();
	checkBox = document.createElement ('input');
	checkBox.type = 'checkbox';
	checkBox.id = 'cb_'+ id;
	listItem.appendChild(checkBox);
	checkBox.onclick = updateItem;
	span = document.createElement ('span');
	span.id ='sp_'+ id;
	listItem.appendChild(span);
	span.innerText = addText.value;
	span.onclick = renameDelete;
	addText.select();
	},false);
	
	//Add with Enter
	addText.addEventListener ('keyup', function(event){
	if(event.which == 13){
	if(addText.value == '' || addText.value == ' '){
		addText.select();
		return false;
	}
	date= new Date();
	id = '' + date.getHours() + date.getMinutes() + date.getSeconds() + date.getMilliseconds();
	listItem = document.createElement ('li');
	listItem.id = 'li_'+ id;
	list.appendChild(listItem);
	checkBox = document.createElement ('input');
	checkBox.type = 'checkbox';
	checkBox.id = 'cb_'+ id;
	listItem.appendChild(checkBox);
	checkBox.onclick = updateItem;
	span = document.createElement ('span');
	span.id ='sp_'+ id;
	listItem.appendChild(span);
	span.innerText = addText.value;
	span.onclick = renameDelete;
	addText.select();
	}
	},false);
}
function updateItem () {
	checkBoxId = this.id.replace ('cb_','');
	spanText = document.getElementById ('sp_' + checkBoxId);
	listItem = document.getElementById ('li_' + checkBoxId);
	if (this.checked){
		spanText.className = 'checked';	
	} else {
		spanText.className = '';
	}
}
function renameDelete () {
	ask = prompt('   RENAME =>R \n\n   DELETE => D\n');
	if (ask == 'R' || ask == 'r' || ask == 'Р' || ask == 'р'){
		rename = prompt('Rename Item');
		this.innerText = rename;
	} 
	if (ask == 'D' || ask == 'd' || ask == 'Д' || ask == 'д'){
		spanId = this.id.replace ('sp_','');
		r = document.getElementById ('li_'+ spanId );
		r.remove(r.selectedIndex);
	}
}
window.onload = function(){
	createNewItem();
}